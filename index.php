<?php
    require_once("animal_php.php");
    require_once("frog.php");
    require_once("ape.php");
    $sheep = new Animal("shaun");

    echo "nama : $sheep->name <br>";
    echo "jumlah kaki : $sheep->legs <br>";
    echo "darah : $sheep->cold_bloods <br><br>";

    $kodok = new Frog("buduk");
    echo "Nama : $kodok->name<br>";
    echo "jumlah kaki : $kodok->legs<br>";
    echo "darah : $kodok->cold_bloods<br>";
    $kodok->jump();

    $kera = new Ape ("kera sakit");
    echo "Nama : $kera->name<br>";
    echo "jumlah kaki : $kera->legs<br>";
    echo "darah : $kera->cold_bloods<br>";
    $kera->yell();

?>