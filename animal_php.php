<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>
    <?php
         
        class Animal{
            public $name;
            public $legs = 2;
            public $cold_bloods = "false";

            public function __construct($string){

                $this->name=$string;
            }
        }

    ?>
</body>
